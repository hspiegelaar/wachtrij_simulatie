import numpy as np

DUR = 3600                  # duur van de run in seconde

m_klant  = 12               # gemiddelde tijd tussen twee klanten in seconden
m_winkel = 120              # gemiddelde verblijftijd in de winkel in seconden
m_kassa  = [60, 30, 60, 60] # gemiddedle tijd aan de kassa in seconden

winkel = []
kassas = [[], [], [], []]

stats = {}

time_volgende_klant = np.random.poisson(m_klant)


def print_kassas(kassas):

    print('{} | {} | {} | {}'.format(len(kassas[0]), len(kassas[1]), len(kassas[2]), len(kassas[3])))


for time_step in range(DUR):

    for kassa_nummer in range(len(kassas)):
        aantal_klanten = len(kassas[kassa_nummer])
        if aantal_klanten in stats: 
            stats[aantal_klanten] += 1
        else:
            stats[aantal_klanten] = 1

    #print_kassas(kassas)

    # voeg klanten aan de winke;-queue toe 
    if time_step == time_volgende_klant:

        winkel.append(time_step + np.random.poisson(m_winkel))
        time_volgende_klant = time_step + np.random.poisson(m_klant)
        print('{:>6} 1: klant in winkel'.format(time_step))


    # kijk of er klanten bij de kassa komen
    for klant_uit in winkel:

        if klant_uit == time_step:

            # zoek de kortste rij
            min = 10000
            k = 0
            n = 0

            for kassa_nummer in range(len(kassas)):
               if len(kassas[kassa_nummer]) <= min:
                   k = kassa_nummer
                   min = len(kassas[kassa_nummer])

            # haal klant uit winkel queue
            winkel.remove(time_step)
            print('{:>6} 2: klant bij kassa {}'.format(time_step, k))


            # zet klant in kassa queue
            if (len(kassas[k]) > 0):
                kassas[k].append(kassas[k][0] + np.random.poisson(m_kassa[k]))
            else:
                kassas[k].append(time_step + np.random.poisson(m_kassa[k]))

    # kijk of er een klant uit een kassa-queue komt
    for kassa in range(len(kassas)):
        for klant_uit in kassas[kassa]:
            if klant_uit == time_step:
                kassas[kassa].remove(time_step)
                print('{:>6} 3: klant naar buiten'.format(time_step))


print(stats)

